# **NEW REPO**

This repo does not host the packages for the integration of 66 into voidlinux anymore.

That does not mean that the project has stopped though. There is a new proper repository that holds all the packages that resided in this one and will be updated with new releases and more packages. You can add it to your system with:

```sh
  # xbps-install --repository=https://avyssos.eu/repos/voidlinux/66/ void-unofficial-repo-66
```

Many thanks to both codeberg and osdn for the service they provide to the free software community!
